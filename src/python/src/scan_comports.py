import time

import serial
from serial.tools import list_ports


def processString(rstring):
	#print(rstring)
	split_rstring = rstring.split('\n')
	
	state = split_rstring[1]
	if not "Halt" in state:
		return
	current_string = split_rstring[2]
	potential_string = split_rstring[3]
	pin13 = split_rstring[4]
	pin12 = split_rstring[5]
	pin11 = split_rstring[6]
	power_str = split_rstring[7]
	error_messages = split_rstring[8]
	eof_string = split_rstring[9]
	

	amps = float(current_string.split(' ')[2])
	volts = float(potential_string.split(' ')[2])
	power = float(power_str.split(' ')[1])
	
	print("Amps: " + str(amps))
	print("Volts: " + str(volts))
	print("W: " + str(power))
	

getlist = []
while True:
	getlist = list_ports.comports()
	if len(getlist) == 0:
		print("No battery tester detected.")
		
	for i in getlist:
		print("Analyzing " + str(i.device))
		#print("data: " + str(i.location))
		ser = None


		if i.manufacturer == 'wch.cn':
			print("Valid!")
			
		try:
			ser = serial.Serial(port=i.device)
			print("???")
		except serial.serialutil.SerialException as s_ex:
			print("Access denied.")
			continue			
		
		try:
			print(i.vid)
			print(i.pid)
			while True:
				rstring = ser.read_until(terminator=b'19538').decode()
				processString(rstring)
		except serial.serialutil.SerialException as s_ex:
			print("Comm interrupted.")
		
			
		ser.close()
		print("done scanning")
		
		
	time.sleep(1)