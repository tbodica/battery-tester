﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

using MonitorVisualizer.Models;

namespace MonitorVisualizer
{
    public partial class MeasurementView : Form
    {
        Series batteryVoltageSeries;
        Series batteryCurrentSeries;

        private List<BatteryMeasurement> measurements;

        public MeasurementView()
        {
            InitializeComponent();
            ConfigureCharts();
        }

        private void ConfigureCharts()
        {
            this.chart1.Series.Clear();

            this.batteryVoltageSeries = new Series();
            this.batteryVoltageSeries.Name = "Battery 1 Voltage";
            this.batteryVoltageSeries.Color = Color.Green;
            this.batteryVoltageSeries.IsXValueIndexed = true;
            this.batteryVoltageSeries.ChartType = SeriesChartType.Line;
            this.batteryVoltageSeries.BorderWidth = 7;

            this.batteryCurrentSeries = new Series();
            this.batteryCurrentSeries.Name = "Battery 1 Current";
            this.batteryCurrentSeries.Color = Color.Blue;
            this.batteryCurrentSeries.IsXValueIndexed = true;
            this.batteryCurrentSeries.ChartType = SeriesChartType.Line;
            this.batteryCurrentSeries.BorderWidth = 7;

            this.chart1.ChartAreas[0].Name = "Voltage";
            this.chart1.Series.Add(this.batteryVoltageSeries);
            this.batteryVoltageSeries.Points.AddXY(0, 0);

            this.chart1.ChartAreas.Add(new ChartArea("Current"));
            this.chart1.Series.Add(this.batteryCurrentSeries);
            this.batteryCurrentSeries.Points.AddXY(0, 0);


            chart1.Series["Battery 1 Voltage"].ChartArea = "Voltage";
            chart1.Series["Battery 1 Current"].ChartArea = "Current";
        }

        public void AddMeasurements(List<BatteryMeasurement> measurements)
        {
            this.measurements = measurements;
        }
        private void MeasurementView_Load(object sender, EventArgs e)
        {
            for (int i = 0; i <= listViewMeasurements.Columns.Count - 1; ++i)
            {
                listViewMeasurements.Columns[i].Width = -2;
            }

            foreach (var measurement in this.measurements)
            {
                ListViewItem item = new ListViewItem(measurement.voltage.ToString());
                item.SubItems.Add(measurement.current.ToString());
                item.SubItems.Add(measurement.power.ToString());
                item.SubItems.Add(((double)(measurement.ticks) / 1000).ToString());

                this.listViewMeasurements.Items.Add(item);
                System.Console.WriteLine(measurement.ticks);

                this.batteryVoltageSeries.Points.AddXY(measurement.ticks / 1000, measurement.voltage);
                this.batteryCurrentSeries.Points.AddXY(measurement.ticks / 1000, measurement.current);

                //this.batteryVoltageSeries.Points.AddXY(measurement.ti, packet.voltage_mv);
                //this.batteryCurrentSeries.Points.AddXY(this.numberTicks, packet.current_a);
            }
        }
    }
}
