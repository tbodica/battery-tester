﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace MonitorVisualizer.Models
{
    class ArduinoPacket
    {
        public int mV { get; set; }
        public double mA { get; set; } 
        public int mode { get; set; }

        public string error { get; set; }

        bool mA_set = false;
        bool mV_set = false;
        bool mode_set = false;


        public ArduinoPacket(string input)
        {
            this.error = "NONE";
            foreach (var line in Regex.Split(input, "\\r\\n"))
            {
                Console.WriteLine("Trying to match line:");
                Console.WriteLine(line);

                // search Current
                var match = Regex.Match(line, "Current is: (-)?[0-9]+\\.[0-9]+ A");
                if (match.Success)
                {
                    Console.WriteLine("SUCCESS REGEX");
                    var parseMatch = Regex.Match(line, "[0-9]+\\.[0-9]+").Value.ToString();
                    this.mA = double.Parse(parseMatch);
                    this.mA_set = true;
                }


                // search Voltage
                match = Regex.Match(line, "Voltage is: [0-9]+ mV");
                if (match.Success)
                {
                    Console.WriteLine("SUCCESS REGEX");
                    this.mV = int.Parse(Regex.Match(line, "[0-9]+").Value);
                    this.mV_set = true;
                }

                // search mode_state
                match = Regex.Match(line, "mode_state: [0-9]");
                if (match.Success)
                {
                    Console.WriteLine("SUCCESS REGEX");
                    this.mode = int.Parse(Regex.Match(line, "[0-9]+").Value);
                    this.mode_set = true;
                }

                // search lastError
                match = Regex.Match(line, "Error messages:");
                if (match.Success)
                {
                    Console.WriteLine("SUCCESS REGEX");
                    this.error = Regex.Match(line, "[^:]*$").Value.ToString();
                }


                Console.WriteLine("DEBUGGING AP");
                Console.WriteLine(mV.ToString());
            }

            if (this.mA_set && this.mV_set && this.mode_set)
            {
                return;
            }
            else
            {
                throw new NotImplementedException("Ow");
            }
        }
    }
}
