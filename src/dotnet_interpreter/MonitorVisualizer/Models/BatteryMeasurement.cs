﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonitorVisualizer.Models
{
    public class BatteryMeasurement
    {
        public double current;
        public double voltage;
        public double power;
        public long ticks;

        public BatteryMeasurement()
        {
            this.current = 0;
            this.voltage = 0;
            this.power = 0;
            this.ticks = 0;
        }

        public BatteryMeasurement(double current, double voltage, double power, long ticks) 
        {
            this.current = current;
            this.voltage = voltage;
            this.power = power;
            this.ticks = ticks;
        }
    }
}
