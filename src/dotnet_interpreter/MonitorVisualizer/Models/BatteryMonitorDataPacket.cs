﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using System.Runtime.CompilerServices;

namespace MonitorVisualizer.Models
{
    class BatteryMonitorDataPacket
    {
        public double voltage_mv { get; private set; }
        public double current_a { get; private set; }
        public double power_w { get; private set; }

        private Dictionary<int, int> pinModes;

        private string errors;

        private int relayMode;

        public BatteryMonitorDataPacket()
        {

        }

        public BatteryMonitorDataPacket(string buffer)
        {
            this.relayMode = 0;
            this.pinModes = new Dictionary<int, int>();
            this.errors = "";

            var splitBuffer = buffer.Split(new string[] {"EOS"}, StringSplitOptions.None);
            var lastEntry = splitBuffer[0];
            List<string> rows = lastEntry.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None).ToList();


            foreach (var row in rows)
            {
                if (row.Contains("Current"))
                {
                    var value = Regex.Match(row, "[0-9.]+ A$");
                    if (value.Success)
                    {
                        var fString = value.ToString();
                        fString = fString.Replace(" A", "");
                        this.current_a = double.Parse(fString);
                    }
                }

                if (row.Contains("Voltage"))
                {
                    var value = Regex.Match(row, "[0-9.]+ mV$");
                    if (value.Success)
                        this.voltage_mv = double.Parse(value.ToString().Replace(" mV", ""));
                }

                if (row.Contains("Power"))
                {
                    var value = Regex.Match(row, "[0-9.]+ W$");
                    if (value.Success)
                        this.power_w = double.Parse(value.ToString().Replace(" W", ""));
                }

                if (row.Contains("Pin"))
                {
                    if (Regex.IsMatch(row, "[012]$") && Regex.IsMatch(row, " [0-9]+ ")) // operator
                    {
                        var value = int.Parse(Regex.Match(row, "[012]$").ToString());
                        var dbg = Regex.Match(row, " [0-9]+ ").ToString();
                        var key = int.Parse(Regex.Match(row, " [0-9]+ ").ToString());
                        
                        this.pinModes[key] = value;
                    }
                }

                if (row.Contains("Relay mode:"))
                {
                    if (Regex.Match(row, "[^ ][0-9]+$") != null)
                    {
                        var value = int.Parse(Regex.Match(row, "[^ ][0-9]+$").ToString());
                        this.relayMode = value;   
                    }
                }

                if (row.Contains("Errors:"))
                {
                    errors += row;
                }
            }
        }

        public override string ToString()
        {
            return this.current_a.ToString() + " A, " + this.voltage_mv.ToString() + " V, " + this.power_w.ToString() + " W.";
        }
    }
}
