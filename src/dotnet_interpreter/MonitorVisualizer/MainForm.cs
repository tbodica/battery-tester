﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Timers;

using MonitorVisualizer.Testing;
using MonitorVisualizer.Models;
using System.Windows.Forms.DataVisualization.Charting;

using MonitorVisualizer.Controllers;
using MonitorVisualizer.Testing;


namespace MonitorVisualizer
{
    public partial class MainForm : Form
    {
        private string currentMeasurementName;

        Series batteryVoltageSeries;
        Series batteryCurrentSeries;

        MeasurementDatabaseController dbController;
        TimedEventSerialEmitter ComData;
        System.Windows.Forms.Timer readingTimer;
        List<BatteryMonitorDataPacket> packetStorage;
        List<BatteryMeasurement> measurementStorage;
        Testing.SerialPortAdruinoReader sr;

        System.Diagnostics.Stopwatch measureTimer;

        bool readingMode;

        private int numberTicks = 0;

        public MainForm()
        {
            InitializeComponent();

            // status label
            this.readingMode = false;
            this.labelStatus.Text = "You can start reading. Waiting...";
            this.labelStatus.ForeColor = Color.Black;

            // COM connection
            this.ComData = new TimedEventSerialEmitter();
            this.packetStorage = new List<BatteryMonitorDataPacket>();
            this.measurementStorage = new List<BatteryMeasurement>();

            // reading timer
            this.readingTimer = new System.Windows.Forms.Timer();
            this.readingTimer.Interval = 1000;
            this.readingTimer.Tick += ReadingCycle;

            this.measureTimer = new System.Diagnostics.Stopwatch();



            // db controller
            this.dbController = new MeasurementDatabaseController();
            try
            {
                this.dbController.CleanCache();
            }
            catch { }
            // chart properties
            ConfigureCharts();

        }

        private void ConfigureCharts()
        {
            this.chart1.Series.Clear();

            this.batteryVoltageSeries = new Series();
            this.batteryVoltageSeries.Name = "Battery 1 Voltage";
            this.batteryVoltageSeries.Color = Color.Green;
            this.batteryVoltageSeries.IsXValueIndexed = true;
            this.batteryVoltageSeries.ChartType = SeriesChartType.Line;
            this.batteryVoltageSeries.BorderWidth = 7;

            this.batteryCurrentSeries = new Series();
            this.batteryCurrentSeries.Name = "Battery 1 Current";
            this.batteryCurrentSeries.Color = Color.Blue;
            this.batteryCurrentSeries.IsXValueIndexed = true;
            this.batteryCurrentSeries.ChartType = SeriesChartType.Line;
            this.batteryCurrentSeries.BorderWidth = 7;

            this.chart1.ChartAreas[0].Name = "Voltage";
            this.chart1.Series.Add(this.batteryVoltageSeries);
            this.batteryVoltageSeries.Points.AddXY(0, 0);

            this.chart1.ChartAreas["Voltage"].AxisX.Maximum = 4;
            this.chart1.ChartAreas["Voltage"].AxisY.Maximum = 4;


            this.chart1.ChartAreas.Add(new ChartArea("Current"));
            this.chart1.Series.Add(this.batteryCurrentSeries);
            this.batteryCurrentSeries.Points.AddXY(0, 0);

            this.chart1.ChartAreas["Current"].AxisX.Maximum = 4;
            this.chart1.ChartAreas["Current"].AxisY.Maximum = 4;

            chart1.Series["Battery 1 Voltage"].ChartArea = "Voltage";
            chart1.Series["Battery 1 Current"].ChartArea = "Current";
        }

        private void ReadingCycle(object sender, EventArgs e)
        {

            this.sr.DebugReadCycle();

            this.numberTicks += 1;
            var currentTimeMS = this.measureTimer.ElapsedMilliseconds;
            System.Console.WriteLine(String.Format("Time: {0}", currentTimeMS));
            //System.Console.WriteLine("Started main form cycle.");
            //var buffer = this.ComData.GetBuffer();
            //if (buffer == null)
            //    return;


            //var packet = new BatteryMonitorDataPacket(buffer);
            //System.Console.WriteLine(packet.current_a);
            //System.Console.WriteLine(packet.ToString());

            //this.packetStorage.Add(packet);

            
            if (!sr.lastSuccess)
            {
                return;
            }
            this.batteryVoltageSeries.Points.AddXY(currentTimeMS/1000, sr.lastPacket.mV);
            this.batteryCurrentSeries.Points.AddXY(currentTimeMS/1000, sr.lastPacket.mA);


            var lastMeasurement = new BatteryMeasurement(sr.lastPacket.mA, sr.lastPacket.mV, sr.lastPacket.mV / 1000 * sr.lastPacket.mA, currentTimeMS);
            this.measurementStorage.Add(lastMeasurement);
            this.dbController.AddToCache(lastMeasurement);

            // todo: fixing this DB insertion


            this.labelVoltsValue.Text = sr.lastPacket.mV.ToString() + " " + "mV";
            this.labelAmpsValue.Text = sr.lastPacket.mA.ToString() + " " + "A";

            // logic for modes, need to update labels and stop in case of bad mode change
            if (sr.mode_state == 0)
            {
                ResetAxes();
                this.labelModeRight.Text = "Idle.";
                return; // this allows to always be able to view the graph, even in case of error
            }
            if (sr.mode_state == 1)
            {
                this.labelModeRight.Text = "Charging.";
            }
            if (sr.mode_state == 2)
            {
                this.labelModeRight.Text = "Testing.";

                // discharging errors only
                // general errors to check outside mode_state checks
                System.Console.WriteLine("Debugging SR problem");
                System.Console.WriteLine(sr.lastError);
                #region discharging_errors
                if (System.Text.RegularExpressions.Regex.IsMatch(sr.lastError, "Battery discharged"))
                {
                    this.buttonStartReading.PerformClick();
                    this.labelStatus.Text = "Battery fully discharged.";
                    return;
                }

                if (System.Text.RegularExpressions.Regex.IsMatch(sr.lastError, "Temperature CRITICAL"))
                {
                    this.buttonStartReading.PerformClick();
                    this.labelStatus.Text = "Stopped due to CRITICAL TEMPERATURE";
                    this.labelStatus.ForeColor = Color.Red;
                    return;
                }

                if (System.Text.RegularExpressions.Regex.IsMatch(sr.lastError, "High current detected"))
                {
                    this.buttonStartReading.PerformClick();
                    this.labelStatus.Text = "Stopped due to High current detected";
                    this.labelStatus.ForeColor = Color.Red;
                    return;
                }
                #endregion
            }

            ResetAxes();
        }

        private void ResetAxes()
        {
            this.chart1.ChartAreas["Voltage"].AxisX.Maximum = Double.NaN;
            this.chart1.ChartAreas["Voltage"].AxisY.Maximum = Double.NaN;
            this.chart1.ChartAreas["Voltage"].RecalculateAxesScale();

            this.chart1.ChartAreas["Current"].AxisX.Maximum = Double.NaN;
            this.chart1.ChartAreas["Current"].AxisY.Maximum = Double.NaN;
            this.chart1.ChartAreas["Current"].RecalculateAxesScale();
        }

        private void buttonStartReading_Click(object sender, EventArgs e)
        {

            if (!readingMode)
            {
                // clear data
                this.numberTicks = 0;
                this.packetStorage.Clear();
                this.measurementStorage.Clear();

                this.currentMeasurementName = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

                this.batteryCurrentSeries.Points.Clear();
                this.batteryVoltageSeries.Points.Clear();

                // todo clear graphs

                this.sr = new Testing.SerialPortAdruinoReader();
                if (!this.sr.StartDetect())
                {
                    // error
                    this.buttonStartReading.Text = "Retry";
                    this.labelStatus.Text = "Failed connecting.";
                    this.labelStatus.ForeColor = Color.Orange;
                    return;
                }
                //this.sr.DebugPrintPorts();

                this.readingTimer.Start();
                this.measureTimer.Start();

                this.buttonStartReading.Text = "Stop Reading";
                this.readingMode = true;

                this.labelStatus.Text = "Started reading.";
                this.labelStatus.ForeColor = Color.Black;
            }
            else
            {
                this.readingTimer.Stop();
                this.measureTimer.Stop();
                this.measureTimer.Reset();
                //this.readingTimer.Dispose();
                //this.readingTimer. = 0;

                Console.WriteLine("Stopped timer at: ");
                Console.WriteLine(measureTimer.ElapsedMilliseconds);
                Console.WriteLine("miliseconds");
                this.buttonStartReading.Text = "Start Reading";
                this.readingMode = false;
                this.labelStatus.Text = "Stopped reading. You can save now.";
                this.labelStatus.ForeColor = Color.Green;
                this.sr.Close();
                this.sr = null;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (readingMode)
            {
                labelStatus.Text = "Can't save while reading!";
                labelStatus.ForeColor = Color.YellowGreen;
                return;
            }

            if (this.numberTicks == 0)
            {
                labelStatus.Text = "Please start a measurement!";
                labelStatus.ForeColor = Color.Green;
                return;
            }

            if (!this.dbController.CheckConnection())
            {
                throw new InvalidOperationException("Database connection failed.");
            }

            var measurementName = this.currentMeasurementName;
            try
            {
                this.dbController.CreateNewMeasurement(measurementName);
            }
            catch
            {
                labelStatus.Text = "This measurement was already saved.";
                labelStatus.ForeColor = Color.Black;
                return;
            }

            foreach (var point in this.batteryVoltageSeries.Points)
            {
                //var volts = 0.0;
                System.Console.WriteLine(point.YValues[point.YValues.Length - 1].ToString());
            }

            
            this.dbController.AddBatchMeasurements(measurementName);
            this.dbController.CleanCache();

            //foreach (var p in this.measurementStorage)
            //{
            //    this.dbController.AddMeasurementData(measurementName, p.current, p.voltage, p.power, p.ticks);
            //}

            this.labelStatus.Text = "Saved!";
            this.labelStatus.ForeColor = Color.Green;

            //foreach (var p in this.packetStorage)
            //{
            //    this.dbController.AddMeasurementData(measurementName, p.current_a, p.voltage_mv, p.power_w, );
            //}
        }

        // todo remove
        private void showMeasurementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var showMeasurementsForm = new MeasurementListForm();
            showMeasurementsForm.AddController(this.dbController);
            showMeasurementsForm.ShowDialog();
        }

        private void showMeasurementsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var showMeasurementsForm = new MeasurementListForm();
            showMeasurementsForm.AddController(this.dbController);
            showMeasurementsForm.ShowDialog();
            showMeasurementsForm.Dispose();
        }
    }
}
