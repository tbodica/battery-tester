﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

using MonitorVisualizer.Models;
using System.Data;

namespace MonitorVisualizer.Controllers
{
    public class MeasurementDatabaseController
    {
        private SQLiteConnection repoConnection;
        private Dictionary<string, bool> settings;
        public List<string> MeasurementNames { get; private set; }
        public MeasurementDatabaseController()
        {
            this.MeasurementNames = new List<string>();
            this.settings = new Dictionary<string, bool>();

            repoConnection = new SQLiteConnection("Data Source=MeasurementsDataBase.sqlite;Version=3;");
            if (!System.IO.File.Exists("MeasurementsDataBase.sqlite"))
            {
                SQLiteConnection.CreateFile("chatDB.sqlite");
                repoConnection.Open();

                string createSettingsTableString = "CREATE TABLE settings (name varchar(128), is_set boolean);";
                SQLiteCommand createSettingsCommand = new SQLiteCommand(createSettingsTableString, repoConnection);
                createSettingsCommand.ExecuteNonQuery();
                repoConnection.Close();
            }

            repoConnection.Open();
            GetSettings();
            GetMeasurementNames();
        }

        private void GetSettings()
        {
            var queryString = "SELECT * from settings";
            var getSettingsCMD = new SQLiteCommand(queryString, this.repoConnection);
            var reader = getSettingsCMD.ExecuteReader();

            while (reader.Read())
            {
                var name = reader.GetString(0);
                var isset = reader.GetBoolean(1);
                this.settings.Add(name, isset);
            }
            reader.Close();
        }
        public bool CheckConnection()
        {
            return repoConnection.State == System.Data.ConnectionState.Open;
        }

        private void GetMeasurementNames()
        {
            this.MeasurementNames.Clear();
            var queryString = "SELECT name FROM sqlite_master WHERE type = 'table' ORDER BY 1";
            var getSettingsCMD = new SQLiteCommand(queryString, this.repoConnection);
            var reader = getSettingsCMD.ExecuteReader();

            while (reader.Read())
            {
                var name = reader.GetString(0);
                if (name != "settings")
                    this.MeasurementNames.Add(name);
            }
            reader.Close();
        }

        private bool MeasurementExists(string measurementTableName)
        {
            var query = String.Format("SELECT COUNT(*) FROM sqlite_master WHERE (type = 'table') AND (name = \'{0}\') LIMIT 1;", measurementTableName);
            var cmd = new SQLiteCommand(query, this.repoConnection);
            var reader = cmd.ExecuteReader();
            var didRead = reader.Read(); // didRead TODO:! clear comments

            //var result = reader.GetString(0);
            if (reader.GetInt32(0) != 0)
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public void CreateNewMeasurement(string measurementTableName)
        {
            if (MeasurementExists(measurementTableName))
                throw new InvalidOperationException("This table already exists.");
            string createSettingsTableString = String.Format("CREATE TABLE \"{0}\" (amps DOUBLE, volts DOUBLE, power DOUBLE, measure_time DATETIME DEFAULT current_timestamp, ticks INT);", measurementTableName);
            SQLiteCommand createSettingsCommand = new SQLiteCommand(createSettingsTableString, repoConnection);
            createSettingsCommand.ExecuteNonQuery();
            GetMeasurementNames();
        }

        // todo: need to optimize the SQL query
        // issue #15
        // this way I can experiment with SQL performance
        // if this fails, just cache it read-time, do some DELETE FROMS
        // optionally timeit
        public void AddBatchMeasurements(string tableName)
        {
            using (SQLiteCommand commitMeasurementsCommand = new SQLiteCommand(this.repoConnection))
            {
                commitMeasurementsCommand.CommandText = String.Format("INSERT INTO \"{0}\" SELECT * FROM cache_table", tableName);
                var reader = commitMeasurementsCommand.ExecuteReader();
                reader.Close();
                commitMeasurementsCommand.Dispose();
            }


        }


        public void CleanCache()
        {
            repoConnection.Close();
            repoConnection = new SQLiteConnection("Data Source=MeasurementsDataBase.sqlite;Version=3;");
            repoConnection.Open();
            GetSettings();
            GetMeasurementNames();
            using (SQLiteCommand cleanupCommand = new SQLiteCommand(this.repoConnection))
            {
                
                cleanupCommand.CommandText = "DROP TABLE cache_table";
                var reader = cleanupCommand.ExecuteReader();
                reader.Close();
                cleanupCommand.Dispose();
            }
        }
        public void AddToCache(BatteryMeasurement measure)
        {
            try
            {
                CreateNewMeasurement("cache_table");
            }
            catch
            {

            }

            if (!MeasurementExists("cache_table"))
                throw new InvalidOperationException("Create table/measurement first!");

            SQLiteCommand insertCommand = new SQLiteCommand(this.repoConnection);
            insertCommand.CommandText = String.Format("INSERT INTO \"{0}\"(amps, volts, power, ticks) VALUES (@ampsValue, @voltsValue, @powerValue, @ticksNumber)", "cache_table");
            insertCommand.Parameters.AddWithValue("ampsValue", measure.current);
            insertCommand.Parameters.AddWithValue("voltsValue", measure.voltage);
            insertCommand.Parameters.AddWithValue("powerValue", measure.power);
            insertCommand.Parameters.AddWithValue("ticksNumber", measure.ticks);
            insertCommand.ExecuteNonQuery();
        }
        // todo possible to refactor with 2 arguments (tableName, MeasurementType)
        public void AddMeasurementData(string tableName, double current, double voltage, double power, long ticksNumber)
        {
            if (!MeasurementExists(tableName))
                throw new InvalidOperationException("Create table/measurement first!");

            SQLiteCommand insertCommand = new SQLiteCommand(this.repoConnection);
            insertCommand.CommandText = String.Format("INSERT INTO \"{0}\"(amps, volts, power, ticks) VALUES (@ampsValue, @voltsValue, @powerValue, @ticksNumber)", tableName);
            insertCommand.Parameters.AddWithValue("ampsValue", current);
            insertCommand.Parameters.AddWithValue("voltsValue", voltage);
            insertCommand.Parameters.AddWithValue("powerValue", power);
            insertCommand.Parameters.AddWithValue("ticksNumber", ticksNumber);
            insertCommand.ExecuteNonQuery();
        }

        public List<BatteryMeasurement> GetReadingsFromMeasurement(string measurementName)
        {
            if (!MeasurementExists(measurementName))
                throw new InvalidOperationException("Measurement does not exist.");

            List<BatteryMeasurement> rval = new List<BatteryMeasurement>();
            var queryString = String.Format("SELECT amps, volts, power, measure_time, ticks FROM \"{0}\" ORDER BY measure_time", measurementName);
            var getSettingsCMD = new SQLiteCommand(queryString, this.repoConnection);
            var reader = getSettingsCMD.ExecuteReader();

            while (reader.Read())
            {
                var amps = reader.GetDouble(0);
                var volts = reader.GetDouble(1);
                var power = reader.GetDouble(2);
                var ticks = reader.GetInt32(4);


                rval.Add(new BatteryMeasurement(amps, volts, power, ticks));
            }
            reader.Close();

            return rval;
        }
    }
}
