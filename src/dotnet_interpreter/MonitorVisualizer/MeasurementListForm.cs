﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MonitorVisualizer.Controllers;

namespace MonitorVisualizer
{
    public partial class MeasurementListForm : Form
    {
        MeasurementDatabaseController measurementDatabaseController;
        public MeasurementListForm()
        {
            InitializeComponent();
        }

        public void AddController(MeasurementDatabaseController measurementDatabaseController)
        {
            this.measurementDatabaseController = measurementDatabaseController;
        }

        private void MeasurementListForm_Load(object sender, EventArgs e)
        {
            for (int i = 0; i <= listViewMeasurements.Columns.Count - 1; ++i)
            {
                listViewMeasurements.Columns[i].Width = -2;
            }

            var index = 0;
            foreach (var table in this.measurementDatabaseController.MeasurementNames)
            {
                var newItem = new ListViewItem(index.ToString());
                newItem.SubItems.Add(table);
                if (table == "cache_table")
                    continue;
                this.listViewMeasurements.Items.Add(newItem);
                //this.listViewMeasurements.Items.
                index += 1;
            }
        }

        private void listViewMeasurements_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListViewHitTestInfo hitTestInfo = listViewMeasurements.HitTest(e.X, e.Y);
            ListViewItem item = hitTestInfo.Item;

            var col = hitTestInfo.Item.Index;
            Console.WriteLine(col);

            //item.SubItems[]
            System.Console.WriteLine("Double clicked: " + (item.SubItems[1].Text));

            MeasurementView measurementView = new MeasurementView();
            //var extractedMeasurements = this.measurementDatabaseController.GetReadingsFromMeasurement
            measurementView.AddMeasurements(this.measurementDatabaseController.GetReadingsFromMeasurement(item.SubItems[1].Text));
            measurementView.ShowDialog();
        }
    }
}
