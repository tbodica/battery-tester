﻿namespace MonitorVisualizer
{
    partial class MeasurementListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewMeasurements = new System.Windows.Forms.ListView();
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderIndex = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listViewMeasurements
            // 
            this.listViewMeasurements.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderIndex,
            this.columnHeaderName});
            this.listViewMeasurements.HideSelection = false;
            this.listViewMeasurements.Location = new System.Drawing.Point(23, 32);
            this.listViewMeasurements.Name = "listViewMeasurements";
            this.listViewMeasurements.Size = new System.Drawing.Size(606, 317);
            this.listViewMeasurements.TabIndex = 0;
            this.listViewMeasurements.UseCompatibleStateImageBehavior = false;
            this.listViewMeasurements.View = System.Windows.Forms.View.Details;
            this.listViewMeasurements.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewMeasurements_MouseDoubleClick);
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Measurement Name";
            this.columnHeaderName.Width = 130;
            // 
            // columnHeaderIndex
            // 
            this.columnHeaderIndex.Text = "No. ";
            // 
            // MeasurementListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listViewMeasurements);
            this.Name = "MeasurementListForm";
            this.Text = "Measurements";
            this.Load += new System.EventHandler(this.MeasurementListForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewMeasurements;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderIndex;
    }
}