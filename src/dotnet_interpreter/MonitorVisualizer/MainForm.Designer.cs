﻿namespace MonitorVisualizer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonStartReading = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.labelRowText1 = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showMeasurementsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.labelModeLeft = new System.Windows.Forms.Label();
            this.labelModeRight = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelVoltsValue = new System.Windows.Forms.Label();
            this.labelAmpsValue = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(12, 58);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(776, 426);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chartBattery1";
            // 
            // buttonStartReading
            // 
            this.buttonStartReading.Location = new System.Drawing.Point(48, 577);
            this.buttonStartReading.Name = "buttonStartReading";
            this.buttonStartReading.Size = new System.Drawing.Size(110, 23);
            this.buttonStartReading.TabIndex = 1;
            this.buttonStartReading.Text = "Start Reading";
            this.buttonStartReading.UseVisualStyleBackColor = true;
            this.buttonStartReading.Click += new System.EventHandler(this.buttonStartReading_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(48, 606);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(110, 23);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // labelRowText1
            // 
            this.labelRowText1.AutoSize = true;
            this.labelRowText1.Location = new System.Drawing.Point(310, 577);
            this.labelRowText1.Name = "labelRowText1";
            this.labelRowText1.Size = new System.Drawing.Size(40, 13);
            this.labelRowText1.TabIndex = 3;
            this.labelRowText1.Text = "Status:";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(367, 577);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(0, 13);
            this.labelStatus.TabIndex = 4;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(814, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showMeasurementsToolStripMenuItem1});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // showMeasurementsToolStripMenuItem1
            // 
            this.showMeasurementsToolStripMenuItem1.Name = "showMeasurementsToolStripMenuItem1";
            this.showMeasurementsToolStripMenuItem1.Size = new System.Drawing.Size(184, 22);
            this.showMeasurementsToolStripMenuItem1.Text = "Show Measurements";
            this.showMeasurementsToolStripMenuItem1.Click += new System.EventHandler(this.showMeasurementsToolStripMenuItem1_Click);
            // 
            // labelModeLeft
            // 
            this.labelModeLeft.AutoSize = true;
            this.labelModeLeft.Location = new System.Drawing.Point(310, 606);
            this.labelModeLeft.Name = "labelModeLeft";
            this.labelModeLeft.Size = new System.Drawing.Size(37, 13);
            this.labelModeLeft.TabIndex = 6;
            this.labelModeLeft.Text = "Mode:";
            // 
            // labelModeRight
            // 
            this.labelModeRight.AutoSize = true;
            this.labelModeRight.Location = new System.Drawing.Point(367, 606);
            this.labelModeRight.Name = "labelModeRight";
            this.labelModeRight.Size = new System.Drawing.Size(0, 13);
            this.labelModeRight.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 491);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Voltage:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 518);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Current:";
            // 
            // labelVoltsValue
            // 
            this.labelVoltsValue.AutoSize = true;
            this.labelVoltsValue.Location = new System.Drawing.Point(140, 491);
            this.labelVoltsValue.Name = "labelVoltsValue";
            this.labelVoltsValue.Size = new System.Drawing.Size(0, 13);
            this.labelVoltsValue.TabIndex = 10;
            // 
            // labelAmpsValue
            // 
            this.labelAmpsValue.AutoSize = true;
            this.labelAmpsValue.Location = new System.Drawing.Point(140, 518);
            this.labelAmpsValue.Name = "labelAmpsValue";
            this.labelAmpsValue.Size = new System.Drawing.Size(0, 13);
            this.labelAmpsValue.TabIndex = 11;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 665);
            this.Controls.Add(this.labelAmpsValue);
            this.Controls.Add(this.labelVoltsValue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelModeRight);
            this.Controls.Add(this.labelModeLeft);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelRowText1);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonStartReading);
            this.Controls.Add(this.chart1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Main Control";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button buttonStartReading;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label labelRowText1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showMeasurementsToolStripMenuItem1;
        private System.Windows.Forms.Label labelModeLeft;
        private System.Windows.Forms.Label labelModeRight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelVoltsValue;
        private System.Windows.Forms.Label labelAmpsValue;
    }
}

