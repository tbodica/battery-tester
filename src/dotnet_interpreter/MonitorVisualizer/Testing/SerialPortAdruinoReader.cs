﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

using System.Text.RegularExpressions;

namespace MonitorVisualizer.Testing
{
    class SerialPortAdruinoReader
    {
        public MonitorVisualizer.Models.ArduinoPacket lastPacket { get; set; }
        public bool lastSuccess { get; set; }
        SerialPort arduinoCom;
        List<string> portsList;
        public int mode_state { get; set; }
        public string lastError { get; set; }

        public SerialPortAdruinoReader()
        {
            portsList = new List<string>();

            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                Console.WriteLine(port);
                this.portsList.Add(port);
            }

            this.lastError = "NONE";
        }

        public void DebugPrintPorts()
        {
            string[] ports = SerialPort.GetPortNames();

            Console.WriteLine("List: ");

            foreach (string port in ports)
            {
                Console.WriteLine(port);
                //this.portsList.Add(port);
            }

        }

        public void DebugReadCycle()
        {
            string dst = "";
            byte[] buf = new byte[15000];

            this.arduinoCom.ReadTimeout = 750;
            var readCount = 0;
            try
            {
                readCount = this.arduinoCom.Read(buf, 0, 10000);
            }
            catch
            {
                return;
            }

            foreach(byte c in buf)
            {
                char cc = (char)(c);
                dst += cc;
            }

            this.lastSuccess = false;
            foreach (var item in Regex.Split(dst, ("19538")))
            {
                Console.WriteLine("S----");
                Console.WriteLine(item);
                try
                {
                    var ap = new MonitorVisualizer.Models.ArduinoPacket(item);
                    lastPacket = ap;
                    Console.WriteLine(item);
                    Console.WriteLine("SUCCESS INIT");
                    this.lastSuccess = true;
                    this.mode_state = ap.mode;
                    this.lastError = ap.error;
                    break;
                }
                catch
                {
                    continue;
                }

                Console.WriteLine("E----");
            }
            //Console.WriteLine(dst);
        }

        public void Close()
        {
            this.arduinoCom.Close();
        }

        public bool TryPair(string Name)
        {
            Console.WriteLine("Trying pair");
            SerialPort port = new SerialPort(Name, 9600, Parity.None, 8, StopBits.One);
            port.Open();
            byte[] buf = new byte[20000];

            port.ReadTimeout = 750;
            var readCount = 0;
            try
            {
                port.ReadExisting();
                readCount = port.Read(buf, 0, 15000);
            }
            catch
            {
                port.Close();
                return false;
            }
            if (readCount == 0)
            {
                port.Close();
                return false;
            }

            // search in string for char stream identifier
            // join bytes into string
            foreach (char c in buf)
            {
                Console.WriteLine(c);
            }

            arduinoCom = port;
            return true;
            // change return value
        }

        public bool StartDetect()
        {
            if (this.arduinoCom != null)
            {
                return false;
            }

            Console.WriteLine("starting detect");
            foreach (string port in this.portsList)
            {
                Console.WriteLine(port);
                if ( this.TryPair(port) )
                {
                    return true;
                }
            }
            return false;
        }

    }
}
