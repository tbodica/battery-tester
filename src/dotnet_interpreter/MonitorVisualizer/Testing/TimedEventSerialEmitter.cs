﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Timers;

namespace MonitorVisualizer.Testing
{
    class TimedEventSerialEmitter
    {
        public System.Timers.Timer aTimer { get; private set; }
        private string serialBuffer;

        public TimedEventSerialEmitter()
        {
            this.serialBuffer = "";

            this.aTimer = new System.Timers.Timer(3000); // 3 seconds
            this.aTimer.Elapsed += TimerEvent;
            this.aTimer.Start();
        }

        public void TimerEvent(Object source, ElapsedEventArgs e)
        {
            aTimer.Stop();
            aTimer.AutoReset = true;
            aTimer.Start();
            aTimer.Enabled = true;

            var randomGen = new System.Random();
            var rand1 = randomGen.NextDouble();
            var rand2 = randomGen.Next(0, 5000);

            var currentFrame = String.Format(@"
                Pin 13 mode: 1
                Pin 12 mode: 1
                Pin 11 mode: 1
                Current is: {0} A
		        Voltage is: {1} mV
		        Power: 0.5 W
		        Errors: ""No battery detected or charge too low. "", ""A"", ""B""
                Relay mode: 123123
                EOS
                ", rand1, rand2);

            this.serialBuffer += currentFrame;
        }

        public string GetBuffer()
        {
            var rval = this.serialBuffer;
            if (rval == "")
                return null;
            this.serialBuffer = "";
            return rval;
        }
    }
}
