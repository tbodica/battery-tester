#define ACSPin A0
#define voltPin A1


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);

  pinMode(4, INPUT);


  digitalWrite(13, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(11, HIGH);
}


float readCurrent()
{
  static const float vpp = 0.0048828125;
  static const float acsSensitivity = 0.066; // milivolts

  int counts = 1000;
  int reads = 0;

  float totalsum = 0;
  float average = 0;
  
  while (counts--)
  {
    int acsSensorValue = analogRead(A0);
    float sensorVoltage = acsSensorValue * vpp;
    sensorVoltage -= 2.45;
    float amperage = sensorVoltage / acsSensitivity;
    
    totalsum += amperage;
    reads += 1;
  }

  average = totalsum / reads;
//  Serial.print("Average is: ");
//  Serial.println(average);
//  
//  Serial.println(acsSensorValue);
//  Serial.println(sensorVoltage);

  return average;
}

float readVoltage()
{
  static const float mvc = 4.88;
  float counts = 0;
  float mv = 0;

  int measure_counts = 1000;
  int reads = 0;


  float totalsum = 0;
  float averageMilivolts = 0;
  while (measure_counts--) 
  {
    int voltageRead = analogRead(A1);
    
    counts = voltageRead;
    mv = counts * mvc;

    reads += 1;
    totalsum += mv;
  }
  
  averageMilivolts = totalsum / reads;
  
  return averageMilivolts;
}

float averageCurrent = 0;
float measurements = 0;
float currentSums = 0;

bool HALT = false;

void stopCircuit()
{ 
  digitalWrite(13, HIGH);
  digitalWrite(12, HIGH);
  digitalWrite(11, HIGH);
}

void chargingBranch()
{
  digitalWrite(12, HIGH);
  digitalWrite(13, HIGH);
  digitalWrite(11, LOW);
}

void dischargingBranch()
{
  digitalWrite(11, HIGH);
  digitalWrite(12, LOW);
  digitalWrite(13, LOW);
}

void printPinModes()
{
  Serial.print("Pin 13 mode: ");
  Serial.println(digitalRead(13));
  Serial.print("Pin 12 mode: ");
  Serial.println(digitalRead(12));
  Serial.print("Pin 11 mode: ");
  Serial.println(digitalRead(11));
}

bool firstLoop = true;


int mode_state = 0;
int modeButton1 = 0;
bool button_toggle_flag = false;

void modeButton()
{
  int readpin = digitalRead(4);
  Serial.print("Digital pin: ");
  Serial.println(readpin);

  if (readpin == 1)
    button_toggle_flag = true;

  if (readpin == 0 && button_toggle_flag == true)
  {
    mode_state += 1;
    button_toggle_flag = false;
  }

  if (mode_state == 3)
    mode_state = 0;

  Serial.print("mode_state: ");
  Serial.println(mode_state);
}

void loop() {
  /*
   * Steps:
   *  1. state check
   *  get potential (mV)
   *  get current (A)
   *  
   *  get pin values and set relay mode
   */

  // VARIABLES
  modeButton();
  float accCurrent;
  int digitalVolts;
  
  int pin13;
  int pin12;
  int pin11;

  String modeString = "";
  String relayMode = "";
  String errorString = "";

  if (mode_state == 0)
    stopCircuit();
  else if (mode_state == 1)
    chargingBranch();
  else if (mode_state == 2)
    dischargingBranch();

  if (firstLoop == true)
  {
    firstLoop = false;
    dischargingBranch();
    Serial.println("First loop, checking battery.");
  }
  
  Serial.println("=============");
  // put your main code here, to run repeatedly:
  if (!HALT)
    Serial.println("Halt off.");
  else
  {
    Serial.println("Halt on.");
    stopCircuit();
  }
  /*
   * Read sensor values here.
   */
  accCurrent = readCurrent();
  Serial.print("Current is: ");
  Serial.print(accCurrent);
  Serial.println(" A");


  digitalVolts = readVoltage();
  Serial.print("Voltage is: ");
  Serial.print(digitalVolts);
  Serial.println(" mV");
  /*
   * end of sensor readings.
   */


  pin11 = digitalRead(11);
  pin12 = digitalRead(12);
  pin13 = digitalRead(13);
  
  printPinModes();

  

  /*
   * Set relay mode.
   */
  // if charging
  if (pin11 == 0)
  {
    // discharging relays should be disabled
    if (pin12 != 1 || pin13 != 1)
    {
      stopCircuit();
      errorString += "Invalid relay mode detected.";
    }
    relayMode = "Charging.";
  }
  else
  {
    if (pin12 != 1)
    {
      if (pin13 == 1)
      {
        stopCircuit();
        errorString += "Invalid relay mode detected.";
      }
       relayMode = "Discharging.";
    }
    else if (pin13 == 1)
    {
       relayMode = "None";
    }
  }
  /*
   * End set relay mode.
   */



  if (digitalVolts < 3500 && mode_state == 2)
  {
    //Serial.println("Stopping circuit...");
    HALT = true;
    stopCircuit();
    errorString += "Battery discharged.";
    //mode_state = 0;
    modeString = "Discharged";
  }
  else if (mode_state == 1)
  {
    modeString = "Charging";
    HALT=false;
  }

  Serial.print("Power: ");
  Serial.print(accCurrent * digitalVolts/1000);
  Serial.println(" W");

  /*
   * CRITICAL ERRORS
   */
  if (accCurrent < 0.03 && digitalVolts < 2700)
    errorString += "No battery detected or charge too low.";
  else if (digitalVolts > 2700 && accCurrent < 0.03)
    errorString += "Not discharging";
  else
  {
    // battery in
    if (accCurrent > 1)
    {
      errorString += "High current detected.";
      HALT = true;
      stopCircuit();
    }
    else if (accCurrent < 0)
    {
      errorString += "Reverse current detected.";
      HALT = true;
      stopCircuit();
    }
  }
  
  Serial.print("Error messages: ");
  Serial.println(errorString);


  Serial.print("Relay mode: ");
  Serial.println(String(modeString));

  Serial.print(19538);
  delay(256);
}
