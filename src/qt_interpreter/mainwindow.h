#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include <QObject>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void addPoint(double x, double y);
    void clearData();
    void plot();

private slots:
    void readSerial();
    void errorOccuredHandler();
private:
    Ui::MainWindow *ui;
    QVector<double> qv_x, qv_y;
    QSerialPort *arduino;

    static const quint16 arduino_mega_vendor_id = 6790;
    static const quint16 arduino_mega_product_id = 29987;

    QString arduino_port_name;
    bool arduino_is_available;

    QByteArray serialData;
    QString serialBuffer;

    double time;
};

#endif // MAINWINDOW_H
