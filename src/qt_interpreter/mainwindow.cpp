#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QDebug>
#include <QtWidgets>
#include <QObject>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->plot->addGraph();

    ui->plot->xAxis->setRange(0, 1);
    ui->plot->yAxis->setRange(0, 5100);

    ui->plot->graph(0)->setLineStyle(QCPGraph::lsLine);
    ui->plot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    ui->plot->graph(0)->setBrush(QBrush(QColor(0, 0, 255, 20))); // first graph will be filled with translucent blue

    ui->plot->xAxis->setLabel("Time");
    ui->plot->yAxis->setLabel("mV");


    arduino_is_available = false;
    arduino_port_name = "";
    arduino = new QSerialPort(this);
    serialBuffer = "";

    qDebug() << "Number of available ports: " << QSerialPortInfo::availablePorts().length();

    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        qDebug() << "Has vendor ID: " << serialPortInfo.hasVendorIdentifier();
        if (serialPortInfo.hasVendorIdentifier()) {
            qDebug() << "Vendor ID: " << serialPortInfo.vendorIdentifier();
        }

        qDebug() << "Has product ID: " << serialPortInfo.hasProductIdentifier();
        if (serialPortInfo.hasProductIdentifier()) {
            qDebug() << "Product ID: " << serialPortInfo.productIdentifier();
        }
    }

    foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        if (serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier())
        {
            if (serialPortInfo.vendorIdentifier() == arduino_mega_vendor_id)
            {
                if (serialPortInfo.productIdentifier() == arduino_mega_product_id)
                {
                    arduino_port_name = serialPortInfo.portName();
                    qDebug() << "Found port name: " << arduino_port_name << '\n';
                    arduino_is_available = true;
                }
            }
        }
    }

    if (arduino_is_available)
    {


        // open and configure the COM or serial port
        arduino->setPortName(arduino_port_name);
        qDebug() << "Try catch";
        if (arduino->isOpen())
        {
            arduino->close();
            qDebug() << arduino->error() << '\n';
        }
        if (!arduino->open(QSerialPort::ReadOnly))
        {
            QMessageBox::warning(this, "Open error.", "Can't open COM.");
            qDebug() << arduino->error() << '\n';
        } else {
            qDebug() << "Opened success!";
        }

        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        arduino->setFlowControl(QSerialPort::NoFlowControl);

        QObject::connect(arduino, SIGNAL(readyRead()), this, SLOT(readSerial()));
        QObject::connect(arduino, SIGNAL(readChannelFinished()), this, SLOT(errorOccuredHandler()));
    }
    else
    {
        // error message if not available
        // TODO: show in start bar
        QMessageBox::warning(this, "Port error", "Couldn't find Arduino board....");
    }

    time = 0;
}

MainWindow::~MainWindow()
{
    if (arduino->isOpen()){
        arduino->close();
    }

    delete ui;
}

void MainWindow::addPoint(double x, double y)
{
    qv_x.append(x);
    qv_y.append(y);
    plot(); // TODO: time implementation (s)
}

void MainWindow::clearData()
{
    qv_x.clear();
    qv_y.clear();
    ui->plot->graph(0)->setData(qv_x, qv_y);
    ui->plot->replot();
    ui->plot->update();
}

void MainWindow::plot()
{
    ui->plot->graph(0)->setData(qv_x, qv_y);

    if (time > 0.8 * ui->plot->xAxis->range().upper)
    {
        ui->plot->xAxis->setRange(ui->plot->xAxis->range().lower, ui->plot->xAxis->range().upper * 1.2);
        qDebug() << "Condition met.";
    }
    else
    {
        qDebug() << time << ' ' << ui->plot->xAxis->range().upper << '\n';
    }

    ui->plot->replot();
    ui->plot->update();
}

void MainWindow::readSerial()
{
    QStringList bufferSplit = serialBuffer.split(QString("19538"));

    if (bufferSplit.length() < 2)
    {
        serialData = arduino->readAll();
        if (!arduino->isReadable())
        {
            QMessageBox::warning(this, "Connection lost", "Couldn't find Arduino board....");
        }
        serialBuffer += QString::fromStdString(serialData.toStdString());
    }
    else {
        qDebug() << bufferSplit;

        QStringList lineSplit = bufferSplit[0].split("\r\n");
        qDebug() << lineSplit << '\n';
        qDebug() << lineSplit.length() << '\n';

        if (lineSplit.length() < 12)
            return;

        QString currentString = lineSplit[4];
        if (currentString.contains("Current")) {
            double currentValue = currentString.split(' ')[2].toDouble();
            qDebug() << "Current value: " << currentValue << '\n';

            ui->labelAmpsDisplay->setText(QString::number(currentValue));
        }

        QString voltageString = lineSplit[5];
        if (voltageString.contains("Voltage"))
        {
            double voltageValue = voltageString.split(' ')[2].toDouble();
            qDebug() << "Voltage value: " << voltageValue << '\n';
            ui->labelVoltDisplay->setText(QString::number(voltageValue));
            addPoint(time, voltageValue);
            time += 0.01;
        }
        qDebug() << "this ran....\n";
        QString relayModeString = lineSplit[11];
        if (relayModeString.contains("Relay"))
        {
            QString modeString = "";
            if (relayModeString.split(' ').length() >= 3)
                modeString = relayModeString.split(' ')[2];
            qDebug() << "Mode value: " << modeString << '\n';

            if (modeString.contains("Discharged"))
            {
                QPalette warningPalette;
                //warningPalette.setColor(QPalette::Window, Qt::white);
                warningPalette.setColor(QPalette::WindowText, Qt::red);

                //sample_label->setAutoFillBackground(true);
                ui->labelMessageHidden->setPalette(warningPalette);
                ui->labelMessageHidden->setText("Discharged");
            }
            else if (modeString.contains("Charging")) {
                ui->labelMessageHidden->setText("Charging");
            }
            else {
                QPalette clearPalette;
                clearPalette.setColor(QPalette::WindowText, Qt::black);
                ui->labelMessageHidden->setText("");
                ui->labelMessageHidden->setPalette(clearPalette);
            }
        }

        serialBuffer = "";

    }

}

void MainWindow::errorOccuredHandler()
{
    if (arduino->isOpen()){
        arduino->close();
    }
}


