# battery-tester

![App](/Documentation/Images/app.PNG)


![Current state](/Documentation/Images/board.jpg)


Arduino battery tester circuit(for now), and Qt Windows interface.


## Schematic:



![Schematic](/Documentation/Images/c1vb.png)



## PCB Schematic

![Schematic](/Documentation/Images/pcb_kicad.png)


## Components:

10Ohm 10W Resistor


3 relays per cell


1 ACS712


1 Arduino Mega 2560


	- 2 analog pins
	- 4 digital pins
	- 2 x 5v entries
	- all ground slots
